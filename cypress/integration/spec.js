// @ts-check
describe('page', () => {
  beforeEach(() => {
    cy.visit('index.html')
  })

  it('has h2', () => {
    cy.contains('h2', 'test')
  })

  it('can access iframe', () => {
    cy.get('.test-iframe').then($iframe => {
      const iframe = $iframe.contents();

      const goButton = iframe.find('#goButton')
      cy.wrap(goButton)
        .contains('Do something')
    });
  })
})
